﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GuYuGplot.Entites
{
    public class Enums
    {
        public enum LineType
        {
            Normal,
            BlockLine
        }

        public enum NodeType
        {
            Circle,
            Rect
        }
    }
}
