﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace GuYuGplot.Drawing.Elements
{
    class BlockLine:AbsLineBase,IDrawing
    {
        #region IDrawing 成员

        public void Draw(System.Drawing.Graphics graphics)
        {
            graphics.DrawLine(new Pen(this.Color, this.Width), this.StartLocation, this.EndLocation);

            float mX = (this.EndLocation.X + this.StartLocation.X) / 2;//线条中点
            float mY = (this.EndLocation.Y + this.StartLocation.Y) / 2;//线条中点
            RectangleF rect = new RectangleF(mX - 5, mY - 5, 10, 10);
            graphics.FillEllipse(new SolidBrush(Color.Red), rect);
        }

        #endregion
    }
}
