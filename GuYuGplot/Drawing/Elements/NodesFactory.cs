﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GuYuGplot.Entites;

namespace GuYuGplot.Drawing.Elements
{
    class NodesFactory
    {
        public AbsNodeBase GetNode(Enums.NodeType nodeType)
        {
            switch (nodeType)
            {
                case Enums.NodeType.Circle:
                    return new CircleNode();
                case Enums.NodeType.Rect:
                    return new RectNode();
                default:
                    return new CircleNode();
            }
        }
    }
}
