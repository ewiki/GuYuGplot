﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace GuYuGplot.Drawing
{
    interface IDrawing
    {
        void Draw(Graphics graphics);
    }
}
